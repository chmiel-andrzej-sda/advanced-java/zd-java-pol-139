package pl.sdacademy.zdjavapol139;

import java.util.Objects;

public abstract class Entity implements Speakable, Movable {
	private Location location;

	@Override
	public Location getLocation() {
		return location;
	}

	@Override
	public void setLocation(final Location location) {
		if (location != null) {
			this.location = location;
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Entity entity)) {
			return false;
		}
		return Objects.equals(getLocation(), entity.getLocation());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLocation());
	}

	@Override
	public String toString() {
		return "Entity{" +
				"location=" + location +
				'}';
	}
}
