package pl.sdacademy.zdjavapol139;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public abstract class LivingEntity extends Entity {
	protected int health;
	protected int mana;
	private Inventory inventory = new Inventory();

	public int getHealth() {
		return health;
	}

	public int getMana() {
		return mana;
	}

	@NotNull
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(@NotNull final Inventory inventory) {
		this.inventory = inventory;
	}

	public void setHealth(final int health) {
		if (health >= 0 && health <= 100) {
			this.health = health;
		}
	}

	public void setMana(final int mana) {
		if (mana >= 0 && mana <= 100) {
			this.mana = mana;
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final LivingEntity that)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getHealth() == that.getHealth()
				&& getMana() == that.getMana()
				&& Objects.equals(getInventory(), that.getInventory());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getHealth(), getMana(), getInventory());
	}

	@Override
	public String toString() {
		return "LivingEntity{" +
				"health=" + health +
				", mana=" + mana +
				", inventory=" + inventory +
				"} " + super.toString();
	}
}
