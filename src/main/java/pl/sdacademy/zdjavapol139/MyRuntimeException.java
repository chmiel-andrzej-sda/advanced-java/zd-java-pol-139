package pl.sdacademy.zdjavapol139;

public class MyRuntimeException extends RuntimeException {
	public MyRuntimeException(final String message) {
		super(message);
	}
}
