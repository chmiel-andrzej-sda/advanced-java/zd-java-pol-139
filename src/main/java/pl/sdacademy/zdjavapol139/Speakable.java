package pl.sdacademy.zdjavapol139;

import org.jetbrains.annotations.NotNull;

public interface Speakable {
	void speak(@NotNull String text);
}
