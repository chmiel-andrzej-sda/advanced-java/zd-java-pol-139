package pl.sdacademy.zdjavapol139.people;

public record Address(String street, String city, String county) {
}
