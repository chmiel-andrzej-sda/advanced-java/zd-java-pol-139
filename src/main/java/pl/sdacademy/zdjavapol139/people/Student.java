package pl.sdacademy.zdjavapol139.people;

import java.util.Objects;

public final class Student extends Person {
	private final int indexNumber;

	public Student(final String firstName, final String lastName, final int indexNumber) {
		super(firstName, lastName);
		this.indexNumber = indexNumber;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	@Override
	public void walk() {
		System.out.println("Student is walking");
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Student student)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getIndexNumber() == student.getIndexNumber();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getIndexNumber());
	}

	@Override
	public String toString() {
		return "Student{" +
				"indexNumber=" + indexNumber +
				"} " + super.toString();
	}
}
