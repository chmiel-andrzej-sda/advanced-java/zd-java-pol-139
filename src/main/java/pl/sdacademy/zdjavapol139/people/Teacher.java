package pl.sdacademy.zdjavapol139.people;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public non-sealed class Teacher extends Person {
	private final List<Subject> subjects = new ArrayList<>();

	public Teacher(final String firstName, final String lastName) {
		super(firstName, lastName);
	}

	public static class Pet {
		private final String name;
		private String subject;

		public Pet(final String name) {
			this.name = name;
		}
	}

	public List<Subject> getSubjects() {
		return new ArrayList<>(subjects);
	}
	public void add(final Subject subject) {
		this.subjects.add(subject);
	}

	@Override
	public void walk() {
		System.out.println("Teacher walks");
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Teacher teacher)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return Objects.equals(getSubjects(), teacher.getSubjects());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getSubjects());
	}

	@Override
	public String toString() {
		return "Teacher{" +
				"subject=" + subjects +
				"} " + super.toString();
	}
}
