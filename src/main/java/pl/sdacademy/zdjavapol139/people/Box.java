package pl.sdacademy.zdjavapol139.people;

import pl.sdacademy.zdjavapol139.MyException;
import pl.sdacademy.zdjavapol139.MyRuntimeException;

public class Box<E> {
	private E e;

	public Box(final E e) {
		this.e = e;
	}

	public E getElement() {
		return e;
	}

	public void setElement(final E e) {
		this.e = e;
	}

	public void throwsRuntimeException() {
		throw new MyRuntimeException("test");
	}

	public void throwsException() throws MyException {
		throw new MyException("test");
	}
}
