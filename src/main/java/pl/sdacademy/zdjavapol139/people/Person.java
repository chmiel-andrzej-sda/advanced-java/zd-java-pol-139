package pl.sdacademy.zdjavapol139.people;

import java.util.Objects;

public abstract sealed class Person implements Walkable, Comparable<Person> permits Student, Teacher {
	private final String firstName;
	private final String lastName;
	private Address address;

	protected Person(final String firstName, final String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Address getAddress() {
		return address;
	}

	private void setAddress(final Address address) {
		this.address = address;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Person person)) {
			return false;
		}
		return Objects.equals(getFirstName(), person.getFirstName())
				&& Objects.equals(getLastName(), person.getLastName())
				&& Objects.equals(getAddress(), person.getAddress());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getFirstName(), getLastName(), getAddress());
	}

	@Override
	public String toString() {
		return "Person{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", address=" + address +
				'}';
	}

	@Override
	public int compareTo(final Person o) {
		return getLastName().compareToIgnoreCase(o.getLastName());
	}
}
