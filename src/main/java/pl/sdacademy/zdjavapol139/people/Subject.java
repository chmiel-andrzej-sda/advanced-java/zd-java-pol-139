package pl.sdacademy.zdjavapol139.people;

public enum Subject {
	HISTORY("History"),
	MATH("Math"),
	BIOLOGY("Biology"),
	CHEMISTRY("Chemistry");

	private final String name;

	Subject(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
