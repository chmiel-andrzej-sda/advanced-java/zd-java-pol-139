package pl.sdacademy.zdjavapol139;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import pl.sdacademy.zdjavapol139.item.ArmorItem;
import pl.sdacademy.zdjavapol139.item.Item;
import pl.sdacademy.zdjavapol139.people.Address;
import pl.sdacademy.zdjavapol139.people.Box;
import pl.sdacademy.zdjavapol139.people.Person;
import pl.sdacademy.zdjavapol139.people.Student;
import pl.sdacademy.zdjavapol139.people.Subject;
import pl.sdacademy.zdjavapol139.people.Teacher;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Main {
	private static final Random RANDOM = new Random();
	private static final Scanner SCANNER = new Scanner(System.in);

	public static void main(String[] args) throws IOException, InterruptedException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
		Address address = new Address("Kolorowa", "Krakow", "PL");
		System.out.println(address);

		Person person = new Student("Andrzej", "Chmiel", 1);
//		person.setAddress(address);

		Teacher teacher = new Teacher("Abc", "Test");
		teacher.add(Subject.BIOLOGY);

		person.walk();

		if (teacher instanceof Teacher) {
			// fhgfgh
		}

		printName(person);
		printName(teacher);

		Person[] arr = {person, teacher};

		for (Person person1 : arr) {
			if (person1 instanceof final Teacher t) {
				System.out.println(t.getSubjects());
			}
			System.out.println(person1.getLastName());
		}

//		System.out.println(person.getAddress().city());
		System.out.println(teacher);

		Entity entity = new Animal();
		entity.speak("Hello");

		Entity e = new Entity() {
			@Override
			public void speak(@NotNull final String text) {
				System.out.println("test");
			}
		};
		e.speak("");

		Speakable speakable = new Speakable() {
			@Override
			public void speak(@NotNull final String text) {
				System.out.println("cokolwiek: " + text);
			}
		};
		speakable.speak("hello");

		Player player = new Player("Abc");
		NPC npc = new NPC();
		Entity entity1 = new Entity() {
			@Override
			public void speak(@NotNull final String text) {
				System.out.println("entity: " + text);
			}
		};
		Entity[] entities = {player, npc, entity1};
		for (Entity value : entities) {
			System.out.println(value.getClass().getSimpleName());
			value.speak("foreach");
		}

		Entity[] entities1 = new Entity[4];
		for (int i = 0; i < entities1.length; i++) {
			final int index = i;
			entities1[i] = new Entity() {
				@Override
				public void speak(@NotNull final String text) {
					System.out.printf("array %d: %s%n", index, text);
				}
			};
		}

		for (Entity entity2 : entities1) {
			entity2.speak("fore");
		}

		Box<String> x = new Box<>("Xyz");
		System.out.println(x.getElement().length());
//		test(arr[1], arr[0]);

		Inventory inventory = new Inventory();
		Item test = new Item("test");
		test.add(int.class, new Metadata<>(1));
		inventory.add(test);
		inventory.add(new Item("test2"));

		Metadata<Integer> integerMetadata = test.get(int.class);

		for (Item item : inventory.getItems()) {
			Map<Class<?>, Metadata<?>> metadataMap = item.getMetadata();
			for (Map.Entry<Class<?>, Metadata<?>> entry : metadataMap.entrySet()) {
				System.out.println(entry.getKey());
				System.out.println(entry.getValue());
			}
		}
		inventory.getItems()
				.stream()
				.map(Item::getMetadata)
				.map(Map::entrySet)
				.flatMap(Collection::stream)
				.map(entry -> String.format("%s - %s", entry.getKey(), entry.getValue()))
				.forEach(System.out::println);

                /*
                    1 2
                    3 4

                    1 2 3 4
                 */
		test(player);
		Teacher.Pet pet = new Teacher.Pet("");
//		System.out.println(inventory.getItem(10).getName());
		if (player.getHelmet() != null) {
			System.out.println(player.getHelmet().getName());
		}
//		System.out.println(inventory.getItem(-1));

		Box<Integer> box = new Box<>(1);
//		box.throwsRuntimeException();
		try {
			box.throwsException();
		} catch (MyException ex) {
			System.out.println("exception thrown");
		}

		readInt();

//		throwsRegularRuntimeException();
//		try {
//			throwsRegularException();
//		} catch (ReflectiveOperationException ex) {
//			//
//		}
//
//		throwsMyRuntimeException();
//		try {
//			throwsMyException();
//		} catch (MyException ex) {
//			//
//		}
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(1);
		list.add(1);
		list.add(2);
		list.add(3);
//		list.add(null);
		list.remove((Object) 1);
		while (list.contains(1)) {
			list.remove((Object) 1);
		}

		list.forEach(System.out::println);

		getList().add("");

		Set<Person> set = new TreeSet<>(new Comparator<Person>() {
			@Override
			public int compare(final Person o1, final Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
		});
		set.add(new Student("Karol", "chmiel", 1));
		set.add(person);
		System.out.println(set);

		Set<Player> playersHashSet = new HashSet<>();
		Set<Player> playersTreeSet = new TreeSet<>();

		playersHashSet.add(player);
		playersHashSet.add(player);
		playersHashSet.add(player);
		playersHashSet.add(new Player("Test 1"));
		playersHashSet.add(new Player("Test 1"));
		playersTreeSet.add(player);
		playersTreeSet.add(player);
		playersTreeSet.add(player);
		playersTreeSet.add(new Player("Test 1"));
		playersTreeSet.add(new Player("Test 1"));

		for (Player player1 : playersHashSet) {
			System.out.println(player1.getName());
		}
		for (Player player1 : playersTreeSet) {
			System.out.println(player1.getName());
		}

		List<Player> playerList = List.of(player, player, player);
		List<Player> resultList = new ArrayList<>(new HashSet<>(playerList));
		System.out.println(resultList);

		Map<String, Player> map = new HashMap<>();
		map.put("test", player);
		map.put("test 2", player);
		map.put("test", null);
		System.out.println(map);
		map.remove("test");
		System.out.println(map.containsKey("test"));

		for (Map.Entry<String, Player> stringPlayerEntry : map.entrySet()) {
			System.out.println("key = " + stringPlayerEntry.getKey());
			System.out.println("val = " + stringPlayerEntry.getValue());
		}

		Map<Class<?>, Integer> y = new HashMap<>();
		y.put(Student.class, 1);

		// functional programming
		Function<String, Integer> len = String::length;
		System.out.println(len.apply("abc"));
		Consumer<Integer> c = System.out::println;
		c.accept(45);
		Supplier<String> s = () -> "Test";
		System.out.println(s.get());
		Predicate<Integer> p = i -> i > 0;
		System.out.println(p.test(10));

		IntUnaryOperator sum = integer -> {
			int result = 0;
			for (int i = 0; i <= integer; i++) {
				result += i;
			}
			return result;
		};
		System.out.println(sum.applyAsInt(10));

		Supplier<Integer> r = () -> RANDOM.nextInt(11);
		System.out.println(r.get());

		Consumer<String> letters = str -> {
			for (char c1 : str.toCharArray()) {
				System.out.println(c1);
			}
		};
		letters.accept("Test");

		IntPredicate prime = integer -> {
			for (int i = 2; i < integer / 2; i++) {
				if (integer % i == 0) {
					return false;
				}
			}
			return true;
		};

		System.out.println(prime.test(103));

		List<Integer> collect = Stream.of(5, 8, 1, 4, 9, 10, -2, 3, 9)
				.distinct()
				.filter(integer -> integer % 2 == 0)
				.filter(integer -> integer > 0)
				.peek(System.out::println)
				.map(i -> i * i)
				.toList();
		System.out.println(collect);

		collect.stream()
				.filter(i -> i < 50)
				.forEach(System.out::println);

		int[] array = {1, 2, 3};
		Arrays.stream(array)
				.mapToObj(integer -> integer)
				.mapToInt(integer -> integer)
				.forEach(System.out::println);

		Stream.of(5, 8, 1, 4, 9, 10, -2, 3, 9)
				.forEach(System.out::println);

		Stream.of(3, 5, 1, 9, 4, 2, 9, 10, 12)
				.filter(integer -> integer % 2 == 0)
				.forEach(System.out::println);

//		player.getInventory()
//				.getItems()
//				.stream()
//				.map(Item::getName)
//				.forEach(System.out::println);

		System.out.println(player.getInventory()
				.getItems()
				.stream()
				.mapToInt(Item::getValue)
				.sum());

		player.getInventory()
				.getItems()
				.stream()
				.filter(item -> item.getMetadata().isEmpty())
				.forEach(System.out::println);

		playersHashSet.stream()
				.filter(player1 -> (player1.getLeggins() == null)
						|| (player1.getBoots() == null)
						|| (player1.getHelmet() == null)
						|| (player1.getChestplate() == null))
				.forEach(System.out::println);

		playersHashSet.stream()
				.filter(player1 ->
						Stream.of(player1.getLeggins(),
										player1.getBoots(),
										player1.getHelmet(),
										player1.getChestplate())
								.anyMatch(Objects::isNull))
				.forEach(System.out::println);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.filter(it -> it.getName().startsWith("X"))
				.findFirst();

		Optional.ofNullable(null)
				.map(Object::toString)
				.ifPresent(System.out::println);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
//				.map(l -> l.get(0))
				.ifPresent(System.out::println);

		System.out.println(Optional.of(player)
				.map(Player::getHelmet)
				.map(ArmorItem::getMaterial)
				.orElse(null));

//		System.out.println(Optional.ofNullable(player)
//				.map(Player::getGold)
//				.filter(g -> g != 0)
//				.orElseThrow(IllegalArgumentException::new));

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.mapToInt(Item::getValue)
				.average()
				.ifPresent(System.out::println);

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.map(Item::getMetadata)
				.mapToInt(Map::size)
				.sum();

		Optional.of(player)
				.map(LivingEntity::getInventory)
				.map(Inventory::getItems)
				.stream()
				.flatMap(Collection::stream)
				.filter(it -> !it.getMetadata().isEmpty())
				.forEach(System.out::println);

		// java io

		File file = new File("/Users");
		File[] files = file.listFiles();
		System.out.println(file.getAbsolutePath());
		Arrays.stream(files).sorted().forEach(System.out::println);
//		System.out.println(Arrays.toString(files));

		Path path = Paths.get("pom.xml");
		List<String> strings = Files.readAllLines(path);
		System.out.println(strings);

		try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		Files.createDirectories(Paths.get("files"));
		Path files1 = Paths.get("files", "file.txt");
		try (PrintWriter printWriter = new PrintWriter(files1.toFile())) {
			printWriter.write("Test\n");
		}

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(files1.toFile(), true))) {
			writer.append("Test 1234\n");
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		Player filePlayer1 = new Player("Andret");
		filePlayer1.setGold(23);
		Player filePlayer2 = new Player("Felix");
		filePlayer2.setGold(1024);
		Player filePlayer3 = new Player("Net");
		filePlayer3.setGold(-40);

		Files.createDirectories(Paths.get("players"));
		Stream.of(filePlayer1, filePlayer2, filePlayer3)
				.forEach(player1 -> {
					Path playerFile = Paths.get("players", player1.getName() + ".txt");
					try (PrintWriter printWriter = new PrintWriter(playerFile.toFile())) {
						printWriter.write(player1.toString());
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				});

		File[] playerFiles = new File("players").listFiles();
		Arrays.stream(playerFiles).sorted().forEach(System.out::println);
		System.out.println(readFile("Net"));

		// threads

		Thread thread1 = new Thread(() -> System.out.println("hello world 1"));
		thread1.start();
		System.out.println("hello main thread");

		Thread thread2 = new Thread(() -> Stream.iterate(1, i -> i + 1)
				.limit(20)
				.forEach(System.out::println));

		Thread thread3 = new Thread(() -> Stream.generate(() -> RANDOM.nextInt(100))
				.limit(10)
				.forEach(x1 -> {
					try {
						Thread.sleep(500);
					} catch (InterruptedException ex) {
						Thread.currentThread().interrupt();
					}
					System.out.println(x1);
				}));

		Thread thread4 = new Thread(() -> {
			int number = SCANNER.nextInt();
			System.out.println("thread 4: " + Math.pow(number, 2) + ", " + Math.pow(number, 3));
		});
//		thread2.start();
//		thread3.start();
//		thread4.start();

		Thread thread5 = new Thread(() -> run(450));
		Thread thread6 = new Thread(() -> run(500));
		Thread thread7 = new Thread(() -> run(550));

//		thread5.start();
//		thread6.start();
//		thread7.start();

		TestResource testResource = new TestResource();

		Thread thread8 = new Thread(() ->
				testResource.setX(testResource.getX() + 1));

		Thread thread9 = new Thread(() ->
				testResource.setX(testResource.getX() + 1));

//		thread8.start();
//		Thread.sleep(250);
//		thread9.start();

		Resource resource = new Resource();

		Thread thread10 = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				resource.add(i);
			}
		});
		Thread thread11 = new Thread(() -> {
			for (int i = 0; i < 10; i++) {
				System.out.println(resource.getList());
			}
		});
//		thread10.start();
//		thread11.start();

		// reflect

		System.out.println(person.getAddress());
		Class<Person> personClass = Person.class;
		Method setAddress = personClass.getDeclaredMethod("setAddress", Address.class);
		setAddress.setAccessible(true);
		setAddress.invoke(person, address);
		System.out.println(person.getAddress());

		Arrays.stream(personClass.getDeclaredMethods())
				.map(Method::getName)
				.forEach(System.out::println);

		Visibilities visibilities = new Visibilities();
		Class<? extends Visibilities> visibilitiesClass = visibilities.getClass();

		Arrays.stream(visibilitiesClass.getDeclaredMethods())
				.forEach(method -> {
					method.setAccessible(true);
					System.out.println(method.getName() + "'s type: " + method.getReturnType());
					try {
						Object invoke = method.invoke(visibilities);
						System.out.println("result: " + invoke);
					} catch (ReflectiveOperationException ex) {
						ex.printStackTrace();
					}
				});

		Arrays.stream(personClass.getDeclaredFields())
				.forEach(System.out::println);

		Field firstName = personClass.getDeclaredField("firstName");
		firstName.setAccessible(true);
		firstName.set(person, "Invalid");
		System.out.println(firstName.get(person));
		System.out.println(firstName.getType());

		reset(person);

		Arrays.stream(Visibilities.class.getDeclaredFields())
				.forEach(field -> {
					MyAnnotation annotation = field.getAnnotation(MyAnnotation.class);
					System.out.println(annotation.number() + ", " + annotation.value());
				});

		Arrays.stream(Visibilities.class.getDeclaredMethods())
				.filter(method -> !method.isAnnotationPresent(Ignore.class))
				.sorted(Comparator.comparingInt(m -> Optional.of(m)
						.map(method -> method.getAnnotation(Priority.class))
						.map(Priority::value)
						.orElse(0)))
				.forEach(method -> {
					method.setAccessible(true);
					String description = Optional.of(method)
							.map(m -> m.getAnnotation(Priority.class))
							.map(Priority::description)
							.orElse("");
					System.out.println("desc = " + description);
					try {
						method.invoke(visibilities);
					} catch (ReflectiveOperationException ex) {
						throw new RuntimeException(ex);
					}
				});
		if (person.getClass().equals(Person.class)) {
		}
		download();
	}

	private static void download() throws IOException {
		URL url = new URL("https://api.unusualcalendar.net/holiday/pl");
		HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
		JSONTokener jsonTokener = new JSONTokener(urlConnection.getInputStream());
		JSONArray jsonArray = new JSONArray(jsonTokener);
		for (int j = 0; j < jsonArray.length(); j++) {
			JSONObject jsonObject = jsonArray.getJSONObject(j);
			JSONArray holidays = jsonObject.getJSONArray("holidays");
			for (int i = 0; i < holidays.length(); i++) {
				JSONObject holiday = holidays.getJSONObject(i);
				System.out.println(holiday.getString("name"));
			}
		}

		JSONObject jsonObject = new JSONObject()
				.put("name", "Andrzej")
				.put("surname", "Chmiel")
				.put("inner", new JSONObject()
						.put("a", 1)
						.put("b", 2));
		System.out.println(jsonObject.toString());
		System.out.println(jsonObject.toString(4));
		PrintWriter pw = new PrintWriter("local-json.json");
		pw.println(jsonObject.toString(4));
		pw.close();
		System.out.println("done!");
	}

	private static void reset(Person person) {
		Stream.concat(
						Arrays.stream(person.getClass().getDeclaredFields()),
						Arrays.stream(person.getClass().getSuperclass().getDeclaredFields()))
				.forEach(field -> {
					field.setAccessible(true);
					try {
						if (field.getType().isAssignableFrom(boolean.class)) {
							field.set(person, false);
						} else if (field.getType().isPrimitive() || field.getType().isAssignableFrom(Number.class)) {
							field.set(person, 0);
						} else {
							field.set(person, null);
						}
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e);
					}
				});
		System.out.println(person);
	}

	private static void run(long x) {
		for (int i = 0; i <= 10; i++) {
			System.out.println(Thread.currentThread().getName() + ": " + i);
			try {
				Thread.sleep(x);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}

	@Nullable
	private static String readFile(String name) {
		try (BufferedReader reader = new BufferedReader(new FileReader(String.format("players/%s.txt", name)))) {
			return reader.readLine();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private static int testAgainst(int @NotNull [] array, Predicate<Integer> predicate) {
		int result = 0;
		for (int i : array) {
			if (predicate.test(i)) {
				result++;
			}
		}
		return result;
	}

	@NotNull
	@Contract(" -> new")
	private static List<String> getList() {
		return new ArrayList<>(List.of("abc"));
	}

	private static void printName(Person person) {
		if (person instanceof Teacher) {
			// fhgfgh
		}
		System.out.println(person.getFirstName() + " " + person.getLastName());
	}

	private static <T extends Speakable> void test(T speakable) {
		speakable.speak("test");
	}

	private static void throwsRegularRuntimeException() {
		throw new IllegalArgumentException();
	}

	private static void throwsRegularException() throws ReflectiveOperationException {
		throw new ReflectiveOperationException();
	}

	private static void throwsMyRuntimeException() {
		throw new MyRuntimeException("");
	}

	private static void throwsMyException() throws MyException {
		throw new MyException("");
	}

	private static int readInt() {
		while (true) {
			try {
				return SCANNER.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("That's not an int");
				SCANNER.nextLine();
			}
		}
	}
}

class TestResource {
	private int x;

	public synchronized int getX() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		System.out.println(Thread.currentThread().getName() + " is getting the value");
		return x;
	}

	public synchronized void setX(final int x) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		System.out.println(Thread.currentThread().getName() + " is setting the value");
		this.x = x;
	}
}

class Resource {
	private final List<Integer> list = new ArrayList<>();

	@NotNull
	public synchronized List<Integer> getList() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		return list;
	}

	public synchronized void add(int integer) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		list.add(integer);
	}
}

class Visibilities {
	@MyAnnotation(123)
	private final int x = 10;

	@Priority(value = 1, description = "public!")
	public void publicMethod() {
		System.out.println("public");
	}

	void packagePrivateMethod() {
		System.out.println("package-private");
	}

	@Ignore
	@Priority(description = "protected!")
	protected void protectedMethod() {
		System.out.println("protected");
	}

	@Priority(value = 10)
	private void privateMethod() {
		System.out.println("private");
	}
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface MyAnnotation {
	int value();

	int number() default 0;
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Priority {
	int value() default 0;

	String description() default "";
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Ignore {

}
