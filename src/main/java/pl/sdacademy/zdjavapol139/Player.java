package pl.sdacademy.zdjavapol139;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sdacademy.zdjavapol139.item.Boots;
import pl.sdacademy.zdjavapol139.item.Chestplate;
import pl.sdacademy.zdjavapol139.item.Helmet;
import pl.sdacademy.zdjavapol139.item.Item;
import pl.sdacademy.zdjavapol139.item.Leggins;

import java.util.Objects;

public class Player extends LivingEntity implements Jumpable, Comparable<Player> {
	private final String name;
	private int level;
	private int gold;
	private Item itemInHand;
	private Helmet helmet;
	private Chestplate chestplate;
	private Leggins leggins;
	private Boots boots;

	public Player(@NotNull final String name) {
		this.name = name;
	}

	@NotNull
	public String getName() {
		return name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(final int level) {
		this.level = level;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(final int gold) {
		this.gold = gold;
	}

	@Nullable
	public Item getItemInHand() {
		return itemInHand;
	}

	public void setItemInHand(@Nullable final Item itemInHand) {
		this.itemInHand = itemInHand;
	}

	@Nullable
	public Helmet getHelmet() {
		return helmet;
	}

	public void setHelmet(@Nullable final Helmet helmet) {
		this.helmet = helmet;
	}

	@Nullable
	public Chestplate getChestplate() {
		return chestplate;
	}

	public void setChestplate(@Nullable final Chestplate chestplate) {
		this.chestplate = chestplate;
	}

	@Nullable
	public Leggins getLeggins() {
		return leggins;
	}

	public void setLeggins(@Nullable final Leggins leggins) {
		this.leggins = leggins;
	}

	@Nullable
	public Boots getBoots() {
		return boots;
	}

	public void setBoots(@Nullable final Boots boots) {
		this.boots = boots;
	}

	@Override
	public void speak(@NotNull final String text) {
		System.out.println(name + ": " + text);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Player player)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getLevel() == player.getLevel()
				&& getGold() == player.getGold()
				&& Objects.equals(getName(), player.getName())
				&& Objects.equals(getItemInHand(), player.getItemInHand())
				&& Objects.equals(getHelmet(), player.getHelmet())
				&& Objects.equals(getChestplate(), player.getChestplate())
				&& Objects.equals(getLeggins(), player.getLeggins())
				&& Objects.equals(getBoots(), player.getBoots());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getName(), getLevel(), getGold(), getItemInHand(), getHelmet(), getChestplate(), getLeggins(), getBoots());
	}

	@Override
	public String toString() {
		return "Player{" +
				"name='" + name + '\'' +
				", level=" + level +
				", gold=" + gold +
				", itemInHand=" + itemInHand +
				", helmet=" + helmet +
				", chestplate=" + chestplate +
				", leggins=" + leggins +
				", boots=" + boots +
				"} " + super.toString();
	}

	@Override
	public void jump() {
		System.out.println(name + " jumps");
	}

	@Override
	public int compareTo(final Player o) {
		return name.compareToIgnoreCase(o.name);
	}
}
