package pl.sdacademy.zdjavapol139;

public interface Movable {
	Location getLocation();

	void setLocation(Location location);
}
