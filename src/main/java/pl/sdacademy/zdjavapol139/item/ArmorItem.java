package pl.sdacademy.zdjavapol139.item;

import java.util.Objects;

public sealed class ArmorItem extends Item
		permits Helmet, Chestplate, Leggins, Boots {
	private int armor;
	private final Material material;
	private final CalculationsInner calculationsInner = new CalculationsInner();
	private final CalculationsNested calculationsNested = new CalculationsNested();

	private class CalculationsInner {
		private int realValue;

		private int calculate() {
			return (int) (realValue * armor / 100.);
		}
	}

	private static class CalculationsNested {
		private int realValue;

		private int calculate(int armor) {
			return (int) (realValue * armor / 100.);
		}
	}

	public ArmorItem(final String name, final Material material) {
		super(name);
		this.material = material;
	}

	public int getArmor() {
		return (int) (armor * material.getDefense());
	}

	public void setArmor(final int armor) {
		if (armor >= 0 && armor <= 100) {
			this.armor = armor;
		}
	}

	public Material getMaterial() {
		return material;
	}

	@Override
	public void setValue(final int value) {
		calculationsInner.realValue = value;
		calculationsNested.realValue = value;
//		this.value = calculationsInner.calculate();
		this.value = calculationsNested.calculate(armor);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final ArmorItem armorItem)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getArmor() == armorItem.getArmor()
				&& getMaterial() == armorItem.getMaterial();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getArmor(), getMaterial());
	}

	@Override
	public String toString() {
		return "ArmorItem{" +
				"armor=" + armor +
				", material=" + material +
				"} " + super.toString();
	}
}
