package pl.sdacademy.zdjavapol139.item;

import java.util.Objects;

public non-sealed class Sword extends Item {
	private final Material material;

	public Sword(final String name, final Material material) {
		super(name);
		this.material = material;
	}

	public Material getMaterial() {
		return material;
	}

	@Override
	public String toString() {
		return "Sword{" +
				"material=" + material +
				"} " + super.toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Sword sword)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		return getMaterial() == sword.getMaterial();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getMaterial());
	}
}
