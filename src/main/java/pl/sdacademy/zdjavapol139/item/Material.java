package pl.sdacademy.zdjavapol139.item;

public enum Material {
	IRON("Iron", 0.3),
	SILVER("Silver", 0.5),
	BRONZE("Bronze", 0.7);

	private final String name;
	private final double defense;

	Material(final String name, final double defense) {
		this.name = name;
		this.defense = defense;
	}

	public String getName() {
		return name;
	}

	public double getDefense() {
		return defense;
	}
}
