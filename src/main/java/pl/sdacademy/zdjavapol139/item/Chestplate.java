package pl.sdacademy.zdjavapol139.item;

public final class Chestplate extends ArmorItem {
	public Chestplate(final String name, final Material material) {
		super(name, material);
	}

	@Override
	public String toString() {
		return "Chestplate{} " + super.toString();
	}
}
