package pl.sdacademy.zdjavapol139.item;

public final class Helmet extends ArmorItem {
	public Helmet(final String name, final Material material) {
		super(name, material);
	}

	@Override
	public String toString() {
		return "Helmet{} " + super.toString();
	}
}
