package pl.sdacademy.zdjavapol139.item;

public final class Leggins extends ArmorItem {
	public Leggins(final String name, final Material material) {
		super(name, material);
	}

	@Override
	public String toString() {
		return "Leggins{} " + super.toString();
	}
}
