package pl.sdacademy.zdjavapol139.item;

import pl.sdacademy.zdjavapol139.Metadata;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public sealed class Item permits ArmorItem, Sword {
	protected String name;
	private String description;
	protected int value;
	private final Map<Class<?>, Metadata<?>> metadata = new HashMap<>();

	public Item(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public void setValue(final int value) {
		this.value = value;
	}

	public <T> void add(Class<T> clazz, Metadata<T> metadatum) {
		metadata.put(clazz, metadatum);
	}

	public void remove(Class<?> clazz) {
		metadata.remove(clazz);
	}

	@SuppressWarnings("unchecked")
	public <T> Metadata<T> get(Class<T> clazz) {
		return (Metadata<T>) metadata.get(clazz);
	}

	public Map<Class<?>, Metadata<?>> getMetadata() {
		return new HashMap<>(metadata);
	}

	@Override
	public String toString() {
		return "Item{" +
				"name='" + name + '\'' +
				", description='" + description + '\'' +
				", value=" + value +
				", metadata=" + metadata +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Item item)) {
			return false;
		}
		return getValue() == item.getValue()
				&& Objects.equals(getName(), item.getName())
				&& Objects.equals(getDescription(), item.getDescription())
				&& Objects.equals(getMetadata(), item.getMetadata());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), getDescription(), getValue(), getMetadata());
	}
}
