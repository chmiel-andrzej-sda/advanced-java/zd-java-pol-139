package pl.sdacademy.zdjavapol139.item;

public final class Boots extends ArmorItem {
	public Boots(final String name, final Material material) {
		super(name, material);
	}

	@Override
	public String toString() {
		return "Boots{} " + super.toString();
	}
}
