package pl.sdacademy.zdjavapol139;

public class MyException extends Exception {
	public MyException(final String message) {
		super(message);
	}
}
