package pl.sdacademy.zdjavapol139;

public record Location(int x, int y) {
	public Location() {
		this(0, 0);
	}
}
