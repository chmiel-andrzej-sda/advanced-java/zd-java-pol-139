package pl.sdacademy.zdjavapol139;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class Animal extends LivingEntity {
	private static final Random RANDOM = new Random();

	@Override
	public void speak(@NotNull final String text) {
		int count = 2 + RANDOM.nextInt(4);
		for (int i = 0; i < count; i++) {
			System.out.println(text);
		}
	}
}
