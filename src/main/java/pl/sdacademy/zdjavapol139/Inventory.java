package pl.sdacademy.zdjavapol139;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pl.sdacademy.zdjavapol139.item.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Inventory {
	private final List<@NotNull Item> items = new ArrayList<>();

	@NotNull
	public List<@NotNull Item> getItems() {
		return new ArrayList<>(items);
	}

	public void add(@NotNull final Item item) {
		items.add(item);
	}

	public void remove(@NotNull final Item item) {
		items.remove(item);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof final Inventory inventory)) {
			return false;
		}
		return Objects.equals(items, inventory.items);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(items);
	}

	@Override
	public String toString() {
		return "Inventory{" +
				"items=" + items +
				'}';
	}
}
